package io.github.elitejynx.BukkitProtect.Events;

import io.github.elitejynx.BukkitProtect.BukkitProtect;
import io.github.elitejynx.BukkitProtect.Protections.ProtectionZone;
import io.github.elitejynx.BukkitProtect.Protections.Region;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockIgniteEvent.IgniteCause;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.world.StructureGrowEvent;

@SuppressWarnings("deprecation")
public class BlockEventHandler implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void BreakHanging(HangingBreakByEntityEvent Event) {
		if (Event.getEntity() == null)
			return;
		if (Event.getRemover() instanceof Player) {
			ProtectionZone Protection = BukkitProtect.Plugin
					.isInsideProtection(Event.getEntity().getLocation());
			if (Protection == null
					|| Protection.userHasType(
							((Player) Event.getRemover()).getUniqueId(),
							BukkitProtect.Plugin.UTBuildBlocks))
				return;
		}
		Event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void HangingPlace(HangingPlaceEvent Event) {
		if (Event.getPlayer() == null)
			return;
		ProtectionZone Protection = BukkitProtect.Plugin
				.isInsideProtection(Event.getBlock().getLocation());
		if (Protection == null
				|| Protection.userHasType(Event.getPlayer().getUniqueId(),
						BukkitProtect.Plugin.UTBuildBlocks))
			return;
		Event.setCancelled(true);
		Event.getPlayer().updateInventory();
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void PistonRetract(BlockPistonRetractEvent Event) {
		if (Event.getBlock() == null)
			return;
		if (Event.getBlock().getType() == Material.PISTON_BASE
				|| Event.getBlock().getWorld()
						.getBlockAt(Event.getRetractLocation()).getType() == Material.AIR)
			return;
		ProtectionZone Zone = BukkitProtect.Plugin.isInsideProtection(Event
				.getBlock().getLocation());
		ProtectionZone Zone2 = BukkitProtect.Plugin.isInsideProtection(Event
				.getRetractLocation());
		if (Zone == null && Zone2 == null)
			return;
		if (Zone == Zone2)
			return;
		if (Zone != null && Zone2 == null)
			return;
		if (Zone != null && Zone2 != null) {
			if (Zone2.userHasType(Zone.getOwner(),
					BukkitProtect.Plugin.UTBuildBlocks))
				return;
		}
		Event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void StructureGrow(StructureGrowEvent Event) {
		if (Event.getBlocks().isEmpty())
			return;
		if (Event.getPlayer() == null)
			return;
		ArrayList<ProtectionZone> Zones = new ArrayList<ProtectionZone>();
		for (BlockState block : Event.getBlocks()) {
			ProtectionZone Zone = BukkitProtect.Plugin.isInsideProtection(block
					.getLocation());
			if (Zone != null)
				Zones.add(Zone);
		}
		boolean insideZone = false;
		for (ProtectionZone Zone : Zones) {
			if (!Zone.getOwner().equals(Event.getPlayer().getUniqueId())) {
				insideZone = true;
			} else if (!Zone.userHasType(Event.getPlayer().getUniqueId(),
					BukkitProtect.Plugin.UTBuildBlocks)) {
				insideZone = true;
			}
		}
		if (insideZone)
			Event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void PistonExtend(BlockPistonExtendEvent Event) {
		if (Event.getBlock() == null)
			return;
		if (Event.getBlocks().isEmpty())
			return;
		ArrayList<Block> Blocks = new ArrayList<Block>();
		Blocks.addAll(Event.getBlocks());
		Blocks.add(Event.getBlock());
		for (Block block : Blocks) {
			boolean insideZone = false;
			ProtectionZone Zone = BukkitProtect.Plugin.isInsideProtection(block
					.getLocation());
			ProtectionZone Zone2 = BukkitProtect.Plugin
					.isInsideProtection(block.getRelative(Event.getDirection())
							.getLocation());
			if (Zone == null && Zone2 == null)
				insideZone = true;
			if (Zone == Zone2)
				insideZone = true;
			if (Zone != null && Zone2 == null)
				insideZone = true;
			if (Zone != null && Zone2 != null) {
				if (Zone2.userHasType(Zone.getOwner(),
						BukkitProtect.Plugin.UTBuildBlocks))
					insideZone = true;
			}
			if (!insideZone)
				Event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void BlockFromTo(BlockFromToEvent Event) {
		if (Event.getBlock() == null)
			return;
		ProtectionZone Protection = BukkitProtect.Plugin
				.isInsideProtection(Event.getBlock().getLocation());
		ProtectionZone Protection2 = BukkitProtect.Plugin
				.isInsideProtection(Event.getToBlock().getLocation());
		if (Protection == null && Protection2 == null)
			return;
		if (Protection == Protection2)
			return;
		if (Protection != null && Protection2 == null)
			return;
		if (Protection != null && Protection2 != null) {
			if (Protection2.userHasType(Protection.getOwner(),
					BukkitProtect.Plugin.UTBuildBlocks))
				return;
		}
		Event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void BlockForm(BlockFormEvent Event) {
		if (Event.getBlock() == null)
			return;
		ProtectionZone Protection = BukkitProtect.Plugin
				.isInsideProtection(Event.getBlock().getLocation());
		if (Protection == null)
			return;
		if (BukkitProtect.Plugin.getOverridingTag("Ice",
				Event.getBlock().getLocation()).equalsIgnoreCase("true")
				&& Event.getNewState().getType() == Material.ICE) {
			Event.setCancelled(true);
		}
		if (BukkitProtect.Plugin.getOverridingTag("Snow",
				Event.getBlock().getLocation()).equalsIgnoreCase("true")
				&& Event.getNewState().getType() == Material.SNOW) {
			Event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void BlockFade(BlockFadeEvent Event) {
		if (Event.getBlock() == null)
			return;
		if (BukkitProtect.Plugin.getOverridingTag("Ice",
				Event.getBlock().getLocation()).equalsIgnoreCase("true")
				&& Event.getBlock().getType() == Material.ICE) {
			Event.setCancelled(true);
		}
		if (BukkitProtect.Plugin.getOverridingTag("Snow",
				Event.getBlock().getLocation()).equalsIgnoreCase("true")
				&& Event.getBlock().getType() == Material.SNOW) {
			Event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void BlockBurn(BlockBurnEvent Event) {
		if (Event.getBlock() == null)
			return;
		if (!BukkitProtect.Plugin.getConfig().getBoolean("FireBurn"))
			Event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void BlockIgnite(BlockIgniteEvent Event) {
		if (Event.getBlock() == null)
			return;
		if (!BukkitProtect.Plugin.getConfig().getBoolean("FireSpread"))
			if (Event.getCause() == IgniteCause.SPREAD) {
				Event.setCancelled(true);
			}
		if (BukkitProtect.Plugin.getOverridingTag("Fire",
				Event.getBlock().getLocation()).equalsIgnoreCase("true")) {
			if (Event.getCause() == IgniteCause.SPREAD) {
				Event.setCancelled(true);
			}
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void BlockBreak(BlockBreakEvent Event) {
		if (Event.getPlayer() == null)
			return;
		ProtectionZone Protection = BukkitProtect.Plugin
				.isInsideProtection(Event.getBlock().getLocation());
		if (Protection == null
				|| Protection.userHasType(Event.getPlayer().getUniqueId(),
						BukkitProtect.Plugin.UTBuildBlocks))
			return;
		Event.setCancelled(true);
		Event.getPlayer().updateInventory();
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void BlockPlace(BlockPlaceEvent Event) {
		if (Event.getPlayer() == null)
			return;
		ProtectionZone Protection = BukkitProtect.Plugin
				.isInsideProtection(Event.getBlock().getLocation());
		if (Protection == null
				|| Protection.userHasType(Event.getPlayer().getUniqueId(),
						BukkitProtect.Plugin.UTBuildBlocks)) {
			if (BukkitProtect.Plugin.getConfig().getBoolean("ProtectChests"))
				if (Event.getBlock().getType() == Material.CHEST) {
					if (!BukkitProtect.Plugin.Protections.containsKey(Event
							.getPlayer().getName())
							|| BukkitProtect.Plugin.Protections.get(
									Event.getPlayer().getName()).isEmpty()) {
						Double DefaultSize = Math.sqrt(BukkitProtect.Plugin
								.getConfig().getDouble("ProtectChestsSize"));
						if (DefaultSize % 1 != 0) {
							BukkitProtect.Plugin
									.getLogger()
									.warning(
											"Your 'ProtectChestsSize' value in the config is not a square number!");
							return;
						}
						Region cube = new Region(Event.getBlock().getLocation()
								.clone().add(DefaultSize, 0, DefaultSize),
								Event.getBlock().getLocation().clone()
										.add(-DefaultSize, 0, -DefaultSize));
						ProtectionZone newProt = new ProtectionZone(cube, Event
								.getPlayer().getUniqueId());
						boolean Intersecting = false;
						for (ArrayList<ProtectionZone> Zones : BukkitProtect.Plugin.Protections
								.values()) {
							for (ProtectionZone Zone : Zones) {
								if (newProt.getCube().zonesIntersect(
										Zone.getCube(), false)) {
									Intersecting = true;
								}
							}
						}
						if (!Intersecting) {
							ArrayList<ProtectionZone> Prots = new ArrayList<ProtectionZone>();
							Prots.add(newProt);
							BukkitProtect.Plugin.Protections.put(Event
									.getPlayer().getWorld(), Prots);
							BukkitProtect.Timers.PlayerSelection.remove(Event
									.getPlayer());
							BukkitProtect.Timers.updateFakeBlocks(Event
									.getPlayer());
						}
					}
				}
			return;
		}
		Event.setCancelled(true);
		Event.getPlayer().updateInventory();
	}

}
