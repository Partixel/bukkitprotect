package io.github.elitejynx.BukkitProtect.Events;

import io.github.elitejynx.BukkitProtect.BukkitProtect;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.world.WorldSaveEvent;

public class WorldEventHandler implements Listener {

	@EventHandler(priority = EventPriority.MONITOR)
	public void WorldSave(WorldSaveEvent Event) {
		BukkitProtect.Plugin.saveDB();
	}
}
