package io.github.elitejynx.BukkitProtect.Events;

import io.github.elitejynx.BukkitProtect.BukkitProtect;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

public class SpamHandler implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void PlayerLogin(PlayerLoginEvent event) {
		if (BukkitProtect.Timers.loginSpam.containsKey(event.getAddress()
				.getHostAddress())) {
			if ((System.currentTimeMillis() - BukkitProtect.Timers.loginSpam
					.get(event.getAddress().getHostAddress())) <= BukkitProtect.Plugin
					.getConfig().getDouble("LimitLogins")) {
				event.disallow(Result.KICK_OTHER,
						"You have been kicked for joining more then once in a short period of time");
				BukkitProtect.Timers.loginSpam.put(event.getAddress()
						.getHostAddress(), System.currentTimeMillis());
			} else {
				BukkitProtect.Timers.loginSpam.remove(event.getAddress()
						.getHostAddress());
			}
		} else {
			BukkitProtect.Timers.loginSpam.put(event.getAddress()
					.getHostAddress(), System.currentTimeMillis());
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void PlayerChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		String lastMessage = "";
		if (BukkitProtect.Timers.playerLastChatMessage.containsKey(player))
			lastMessage = BukkitProtect.Timers.playerLastChatMessage
					.get(player);
		BukkitProtect.Timers.playerLastChatMessage.put(player,
				event.getMessage());
		String message = event.getMessage();
		if (!lastMessage.equalsIgnoreCase(message)) {
			double totalCensored = 0;
			int length = message.length();
			String[] words = message.split(" ");
			for (String word : words) {
				if (word.length() >= BukkitProtect.Plugin.getConfig()
						.getDouble("MaxWordLength")) {
					totalCensored = totalCensored + word.length();
					message = message.replace(word, "****");
				} else {
					double caps = 0;
					int repeated = 1;
					char charac = ' ';
					for (char chara : word.toCharArray()) {
						if (Character.isLetter(chara)
								&& Character.isUpperCase(chara)) {
							caps += 1;
						}
						if (chara == charac) {
							repeated += 1;
						} else {
							charac = chara;
							if (repeated < BukkitProtect.Plugin.getConfig()
									.getDouble("LetterDragging"))
								repeated = 1;
						}
					}

					if (repeated > BukkitProtect.Plugin.getConfig().getDouble(
							"LetterDragging")) {
						totalCensored = totalCensored + word.length();
						message = message.replace(word, "****");
					}

					int percent = (int) Math
							.round((caps / word.length()) * 100);
					if (percent >= BukkitProtect.Plugin.getConfig().getDouble(
							"CapsPercentage")
							&& word.length() > 2) {
						message = message.replace(word, word.toLowerCase());
					}
				}
			}
			words = message.split(" ");
			for (String word : BukkitProtect.Plugin.getConfig().getStringList(
					"BannedWords")) {
				for (String worda : words) {
					if (worda.toLowerCase().contains(word.toLowerCase())) {
						totalCensored = totalCensored + worda.length();
						message = message.replace(worda, "****");
					}
				}
			}
			int percent = (int) Math.round((totalCensored / length) * 100);
			if (percent >= BukkitProtect.Plugin.getConfig().getDouble(
					"CensorLimit"))
				event.setCancelled(true);
			event.setMessage(message);
		} else {
			event.getPlayer().sendMessage(
					ChatColor.RED + "Do not repeat yourself");
			event.setCancelled(true);
		}
		int time = (int) System.currentTimeMillis();
		if (BukkitProtect.Timers.playerLastChat.containsKey(player)) {
			int lastChat = BukkitProtect.Timers.playerLastChat.get(player);
			BukkitProtect.Timers.playerLastChat.put(player, time);
			double chatTime = (time - lastChat) / 1000;
			if (!BukkitProtect.Timers.playerSpam.containsKey(player)) {
				BukkitProtect.Timers.playerSpam.put(player, 0);
				return;
			}
			if (chatTime >= BukkitProtect.Plugin.getConfig().getDouble(
					"ChatSpam")) {
				int spam = BukkitProtect.Timers.playerSpam.get(player);
				BukkitProtect.Timers.playerSpam.put(player, spam - 1);
				return;
			} else {
				if (BukkitProtect.Timers.playerSpam.get(player) >= 2)
					event.setCancelled(true);
			}
			int spam = BukkitProtect.Timers.playerSpam.get(player);
			BukkitProtect.Timers.playerSpam.put(player, spam + 1);
			if (spam == 2)
				player.sendMessage(ChatColor.GOLD + "Do not spam");
			else if (spam == 5)
				player.sendMessage(ChatColor.GOLD + "Last warning");
			else if (spam == 10) {
				player.kickPlayer("Do not spam!");
				BukkitProtect.Plugin.getServer().broadcastMessage(
						player.getDisplayName() + ChatColor.RED
								+ " has been kicked for spamming!");
				event.setCancelled(true);
			}
		} else {
			BukkitProtect.Timers.playerLastChat.put(player, time);
			BukkitProtect.Timers.playerLastChatMessage.put(player, message);
		}
	}

}
