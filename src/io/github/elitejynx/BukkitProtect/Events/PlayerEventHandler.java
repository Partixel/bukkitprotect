package io.github.elitejynx.BukkitProtect.Events;

import io.github.elitejynx.BukkitProtect.BukkitProtect;
import io.github.elitejynx.BukkitProtect.Protections.ProtectionZone;
import io.github.elitejynx.BukkitProtect.Protections.UserType;
import io.github.elitejynx.BukkitProtect.Util.Util;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.entity.Tameable;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Button;
import org.bukkit.material.Lever;
import org.bukkit.material.Openable;
import org.bukkit.material.PressurePlate;

@SuppressWarnings("deprecation")
public class PlayerEventHandler implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void PlayerJoin(PlayerLoginEvent Event) {
		if (Event.getPlayer() == null)
			return;
		BukkitProtect.Plugin.loadUserFromFile(Event.getPlayer().getName());
		if (BukkitProtect.Plugin.getConfig().getBoolean("BuyableLand")) {
			if (!BukkitProtect.Plugin.LandOwned.containsKey(Event.getPlayer()
					.getName())) {
				BukkitProtect.Plugin.LandOwned.put(Event.getPlayer().getName(),
						BukkitProtect.Plugin.getConfig().getInt("StartLand"));
			}
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void HoldItem(PlayerItemHeldEvent Event) {
		if (Event.getPlayer() == null)
			return;
		if (Event.getPlayer().getInventory().getItem(Event.getNewSlot()) != null)
			if (Util.isItemRod(Event.getPlayer().getInventory()
					.getItem(Event.getNewSlot()))) {
				Event.getPlayer()
						.sendMessage(
								"You have "
										+ (BukkitProtect.Plugin.LandOwned
												.get(Event.getPlayer()
														.getName()) - BukkitProtect.Plugin
												.getTotalLandUsed(Event
														.getPlayer()))
										+ " blocks of land");
			}
		if (BukkitProtect.Timers.PlayerSelection.containsKey(Event.getPlayer())) {
			BukkitProtect.Timers.PlayerSelection.remove(Event.getPlayer());
			BukkitProtect.Timers.updateFakeBlocks(Event.getPlayer());
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void InteractPlayer(final PlayerInteractEvent Event) {
		if (Event.getPlayer() == null)
			return;
		if (Event.getAction() == Action.LEFT_CLICK_AIR
				|| Event.getAction() == Action.RIGHT_CLICK_AIR)
			return;
		if (Event.getClickedBlock().getType() == Material.SOIL
				&& Event.getAction() == Action.PHYSICAL) {
			if (!BukkitProtect.Plugin.getConfig().getBoolean(
					"PlayersTrampleCrops")) {
				Event.setCancelled(true);
			}
		}
		ProtectionZone Protection = BukkitProtect.Plugin
				.isInsideProtection(Event.getClickedBlock().getLocation());
		UserType requiredPerm = BukkitProtect.Plugin.UTUseBlocks;
		if (Event.getClickedBlock() != null) {
			if (Event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				if (Event.getClickedBlock().getState().getData() instanceof Openable) {
					requiredPerm = BukkitProtect.Plugin.UTAccess;
				} else if (Event.getClickedBlock().getState().getData() instanceof Lever) {
					Block Attached = Event.getClickedBlock().getRelative(
							((Lever) Event.getClickedBlock().getState()
									.getData()).getAttachedFace());
					if (Util.poweringDoor(Event.getClickedBlock())
							|| Util.poweringDoor(Attached))
						requiredPerm = BukkitProtect.Plugin.UTAccess;
				} else if (Event.getClickedBlock().getState().getData() instanceof Button) {
					Block Attached = Event.getClickedBlock().getRelative(
							((Button) Event.getClickedBlock().getState()
									.getData()).getAttachedFace());
					if (Util.poweringDoor(Event.getClickedBlock())
							|| Util.poweringDoor(Attached))
						requiredPerm = BukkitProtect.Plugin.UTAccess;
				}
			} else if (Event.getAction() == Action.PHYSICAL) {
				if (Event.getClickedBlock().getState().getData() instanceof PressurePlate) {
					if (Util.poweringDoor(Event.getClickedBlock())
							|| Util.poweringDoor(Event.getClickedBlock()
									.getRelative(BlockFace.DOWN)))
						requiredPerm = BukkitProtect.Plugin.UTAccess;
				}
			}
		}
		if (Protection == null
				|| Protection.userHasType(Event.getPlayer().getUniqueId(),
						BukkitProtect.Plugin.UTUseBlocks)
				|| Protection.userHasType(Event.getPlayer().getUniqueId(),
						requiredPerm)) {
			if (Event.getAction() == Action.RIGHT_CLICK_BLOCK)
				if (Event.getPlayer().getItemInHand().getType() != Material.AIR
						&& Event.getPlayer().isSneaking()) {

				} else if (Event.getClickedBlock() != null) {
					if (BukkitProtect.Plugin.getConfig().getBoolean(
							"RightClickIronDoor")) {
						if (Event.getClickedBlock().getType() == Material.IRON_DOOR_BLOCK) {
							if (Event.getClickedBlock()
									.getRelative(BlockFace.DOWN).getType() == Material.IRON_DOOR_BLOCK) {
								if (Event.getClickedBlock()
										.getRelative(BlockFace.DOWN).getData() <= 3) {
									Event.getClickedBlock()
											.getRelative(BlockFace.DOWN)
											.setData(
													(byte) (Event
															.getClickedBlock()
															.getRelative(
																	BlockFace.DOWN)
															.getData() + 4));
								} else {
									Event.getClickedBlock()
											.getRelative(BlockFace.DOWN)
											.setData(
													(byte) (Event
															.getClickedBlock()
															.getRelative(
																	BlockFace.DOWN)
															.getData() - 4));
								}
								Event.getClickedBlock()
										.getWorld()
										.playEffect(
												Event.getClickedBlock()
														.getLocation(),
												Effect.DOOR_TOGGLE, 0);
							} else {
								if (Event.getClickedBlock().getData() <= 3) {
									Event.getClickedBlock().setData(
											(byte) (Event.getClickedBlock()
													.getData() + 4));
								} else {
									Event.getClickedBlock().setData(
											(byte) (Event.getClickedBlock()
													.getData() - 4));
								}
								Event.getClickedBlock()
										.getWorld()
										.playEffect(
												Event.getClickedBlock()
														.getLocation(),
												Effect.DOOR_TOGGLE, 0);
							}
						}
					}
				}
			if (Event.hasBlock()) {
				if (Event.getItem() != null) {
					if (Event.getAction() == Action.RIGHT_CLICK_BLOCK
							&& Event.getPlayer().hasPermission(
									"BukkitProtect.Protection.MakeProtections")) {
						try {
							Bukkit.getServer()
									.getScheduler()
									.scheduleSyncDelayedTask(
											BukkitProtect.Plugin,
											new Runnable() {
												@Override
												public void run() {
													ItemStack Rod = Event
															.getPlayer()
															.getInventory()
															.getItemInHand();
													if (BukkitProtect.Plugin.RodTypes
															.contains(Rod
																	.getType())) {
														if (Util.isItemRod(Rod)) {
															if (Event
																	.getPlayer()
																	.isSneaking()) {
																if (Rod.getAmount() > 1) {
																	Event.getPlayer()
																			.sendMessage(
																					"You must not have more then one "
																							+ Rod.getItemMeta()
																									.getDisplayName()
																							+ " in a stack");
																	return;
																}
																BukkitProtect.Plugin
																		.CornerRod(
																				Event.getPlayer(),
																				Event.getClickedBlock()
																						.getLocation(),
																				Rod);
															}
														} else if (Rod
																.getItemMeta()
																.getLore() == null) {
															if (Event
																	.getPlayer()
																	.isSneaking()) {
																BukkitProtect.Timers
																		.updateFakeBlocks(Event
																				.getPlayer());
															} else {
																BukkitProtect.Plugin
																		.DisplayProtection(
																				Event.getPlayer(),
																				Event.getClickedBlock()
																						.getLocation());
															}
														}
													}
												}
											}, 2);
						} catch (Exception e) {
						}
					}
				}
			}
		} else if (!Protection.userHasType(Event.getPlayer().getUniqueId(),
				requiredPerm)) {
			Event.setUseItemInHand(Result.ALLOW);
			if (Event.hasBlock()) {
				if (Event.getItem() != null) {
					try {
						Bukkit.getServer()
								.getScheduler()
								.scheduleSyncDelayedTask(BukkitProtect.Plugin,
										new Runnable() {
											@Override
											public void run() {
												ItemStack Rod = Event
														.getPlayer()
														.getInventory()
														.getItemInHand();
												if (BukkitProtect.Plugin.RodTypes
														.contains(Rod.getType())) {
													if (Rod.getItemMeta()
															.getLore() == null) {
														if (Event.getPlayer()
																.isSneaking()) {
															BukkitProtect.Timers
																	.updateFakeBlocks(Event
																			.getPlayer());
														} else {
															BukkitProtect.Plugin
																	.DisplayProtection(
																			Event.getPlayer(),
																			Event.getClickedBlock()
																					.getLocation());
														}
													}
												}
											}
										}, 2);
					} catch (Exception e) {
					}
					if (Event.getAction() == Action.RIGHT_CLICK_BLOCK) {
						ArrayList<Integer> ItemsIDs = (ArrayList<Integer>) BukkitProtect.Plugin
								.getConfig().getIntegerList("BlockedItems");
						for (int ID : ItemsIDs) {
							if (Event.getItem().getType().getId() == ID)
								Event.setUseItemInHand(Result.DENY);
						}
					}
				}
			}
			Event.setUseInteractedBlock(Result.DENY);
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void InteractEntity(PlayerInteractEntityEvent Event) {
		if (Event.getPlayer() == null)
			return;
		if (Event.getRightClicked() instanceof Tameable) {
			if (((Tameable) Event.getRightClicked()).getOwner() == null
					|| ((Tameable) Event.getRightClicked()).getOwner() == Event
							.getPlayer())
				return;
		} else {
			ProtectionZone Protection = BukkitProtect.Plugin
					.isInsideProtection(Event.getRightClicked().getLocation());
			if (Protection == null
					|| Protection.userHasType(Event.getPlayer().getUniqueId(),
							BukkitProtect.Plugin.UTEntities))
				return;
		}
		Event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void PlayerBucketFill(PlayerBucketFillEvent Event) {
		if (Event.getPlayer() == null)
			return;
		ProtectionZone Protection = BukkitProtect.Plugin
				.isInsideProtection(Event.getBlockClicked().getLocation());
		if (Protection == null
				|| Protection.userHasType(Event.getPlayer().getUniqueId(),
						BukkitProtect.Plugin.UTBuildBlocks))
			return;
		Event.setCancelled(true);
		Event.getPlayer().updateInventory();
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void PlayerBucketEmpty(PlayerBucketEmptyEvent Event) {
		if (Event.getPlayer() == null)
			return;
		ProtectionZone Protection = BukkitProtect.Plugin
				.isInsideProtection(Event.getBlockClicked().getLocation());
		if (Protection == null
				|| Protection.userHasType(Event.getPlayer().getUniqueId(),
						BukkitProtect.Plugin.UTBuildBlocks))
			return;
		Event.setCancelled(true);
		Event.getPlayer().updateInventory();
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void PlayerCraftItem(CraftItemEvent Event) {
		if (Event.getWhoClicked() == null)
			return;
		if (!(Event.getWhoClicked() instanceof Player))
			return;
		for (int i = 1; i < 10; i++) {
			ItemStack Item = Event.getInventory().getItem(i);
			if (Item != null) {
				if (Util.isItemRod(Item)) {
					Event.setCancelled(true);
					return;
				}
			}
		}
	}
}
