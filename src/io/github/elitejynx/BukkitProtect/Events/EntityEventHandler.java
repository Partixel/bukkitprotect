package io.github.elitejynx.BukkitProtect.Events;

import io.github.elitejynx.BukkitProtect.BukkitProtect;
import io.github.elitejynx.BukkitProtect.Protections.ProtectionZone;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Tameable;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.vehicle.VehicleDamageEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;

@SuppressWarnings("deprecation")
public class EntityEventHandler implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void CreatureSpawn(CreatureSpawnEvent Event) {
		if (Event.getEntity() == null)
			return;
		if (Event.getEntity() instanceof Player)
			return;
		if (Event.getSpawnReason() != SpawnReason.NATURAL)
			return;
		if (BukkitProtect.Plugin.getOverridingTag("EntitySpawn",
				Event.getLocation()).equalsIgnoreCase("true")) {
			Event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void DamageVehicle(VehicleDamageEvent Event) {
		if (Event.getVehicle() == null)
			return;
		if (Event.getAttacker() instanceof Player) {
			ProtectionZone Protection = BukkitProtect.Plugin
					.isInsideProtection(Event.getVehicle().getLocation());
			if (Protection == null
					|| Protection.userHasType(
							((Player) Event.getAttacker()).getUniqueId(),
							BukkitProtect.Plugin.UTEntities))
				return;
		}
		Event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void BreakVehicle(VehicleDestroyEvent Event) {
		if (Event.getVehicle() == null)
			return;
		if (Event.getAttacker() instanceof Player) {
			ProtectionZone Protection = BukkitProtect.Plugin
					.isInsideProtection(Event.getVehicle().getLocation());
			if (Protection == null
					|| Protection.userHasType(
							((Player) Event.getAttacker()).getUniqueId(),
							BukkitProtect.Plugin.UTEntities))
				return;
		}
		Event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void DamageEntity(EntityDamageByEntityEvent Event) {
		if (Event.getEntity() == null)
			return;
		if (Event.getEntity() instanceof Monster)
			return;
		if (Event.getCause() == DamageCause.ENTITY_EXPLOSION
				|| Event.getCause() == DamageCause.BLOCK_EXPLOSION) {
			if (!BukkitProtect.Plugin.getConfig().getBoolean("TNTDamageEntity")
					&& Event.getEntityType() == EntityType.PRIMED_TNT)
				Event.setCancelled(true);
			if (!BukkitProtect.Plugin.getConfig().getBoolean(
					"TNTCartDamageEntity")
					&& Event.getEntityType() == EntityType.MINECART_TNT)
				Event.setCancelled(true);
			if (!BukkitProtect.Plugin.getConfig().getBoolean(
					"CreeperDamageEntity")
					&& Event.getEntityType() == EntityType.CREEPER)
				Event.setCancelled(true);
			if (!BukkitProtect.Plugin.getConfig().getBoolean(
					"GhastDamageEntity")
					&& Event.getEntityType() == EntityType.FIREBALL)
				Event.setCancelled(true);
			if (!BukkitProtect.Plugin.getConfig().getBoolean(
					"WitherDamageEntity")
					&& Event.getEntityType() == EntityType.WITHER)
				Event.setCancelled(true);
			if (!BukkitProtect.Plugin.getConfig().getBoolean(
					"WitherHeadDamageEntity")
					&& Event.getEntityType() == EntityType.WITHER_SKULL)
				Event.setCancelled(true);
			if (!BukkitProtect.Plugin.getConfig().getBoolean(
					"EnderCrystalDamageEntity")
					&& Event.getEntityType() == EntityType.ENDER_CRYSTAL)
				Event.setCancelled(true);
		}
		if (Event.getDamager() == null)
			return;
		Player Attacker = null;
		if (Event.getDamager() instanceof Player)
			Attacker = (Player) Event.getDamager();
		if (Event.getDamager() instanceof Projectile)
			if (((Projectile) Event.getDamager()).getShooter() != null
					&& ((Projectile) Event.getDamager()).getShooter() instanceof Player)
				Attacker = (Player) ((Projectile) Event.getDamager())
						.getShooter();
		if (Attacker == null)
			return;
		if (Event.getEntity() instanceof Tameable) {
			if (((Tameable) Event.getEntity()).getOwner() == Attacker) {
				return;
			} else if (((Tameable) Event.getEntity()).getOwner() == null) {
				ProtectionZone Protection = BukkitProtect.Plugin
						.isInsideProtection(Event.getEntity().getLocation());
				if (Protection == null
						|| Protection.userHasType(Attacker.getUniqueId(),
								BukkitProtect.Plugin.UTEntities))
					return;
			} else if (Event.getEntity() instanceof Wolf) {
				if (BukkitProtect.Timers.isPlayerInPVPWith(Attacker,
						(Player) ((Wolf) Event.getEntity()).getOwner()))
					return;
			}
		} else {
			if (!(Event.getEntity() instanceof Player)) {
				ProtectionZone Protection = BukkitProtect.Plugin
						.isInsideProtection(Event.getEntity().getLocation());
				if (Protection == null
						|| Protection.userHasType(Attacker.getUniqueId(),
								BukkitProtect.Plugin.UTEntities))
					return;
			}
		}
		if (Event.getEntity() instanceof Player) {
			if (BukkitProtect.Timers.isPlayerInPVPWith(
					(Player) Event.getEntity(), Attacker))
				return;
			if (!BukkitProtect.Plugin.getOverridingTag("PVP",
					Event.getEntity().getLocation()).equalsIgnoreCase("true")
					&& !BukkitProtect.Plugin.getOverridingTag("PVP",
							Attacker.getLocation()).equalsIgnoreCase("true"))
				return;
		}

		Event.setCancelled(true);
		Attacker.updateInventory();
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void EntityInteract(EntityInteractEvent Event) {
		if (Event.getEntity() == null || Event.getBlock() == null)
			return;
		if (Event.getEntity() instanceof Projectile) {
			if (((Projectile) Event.getEntity()).getShooter() instanceof Player) {
				ProtectionZone Protection = BukkitProtect.Plugin
						.isInsideProtection(Event.getEntity().getLocation());
				if (Protection == null
						|| Protection.userHasType(((Player) ((Projectile) Event
								.getEntity()).getShooter()).getUniqueId(),
								BukkitProtect.Plugin.UTEntities))
					return;
				if (Event.getBlock().getType() == Material.TRIPWIRE
						|| Event.getBlock().getType() == Material.WOOD_BUTTON
						|| Event.getBlock().getType() == Material.WOOD_PLATE)
					Event.setCancelled(true);
			}
		}
		if (Event.getEntity() instanceof Tameable)
			if (!((Tameable) Event.getEntity()).isTamed()) {
				return;
			} else {
				ProtectionZone Protection = BukkitProtect.Plugin
						.isInsideProtection(Event.getEntity().getLocation());
				if (Protection == null
						|| Protection.userHasType(
								((Tameable) Event.getEntity()).getOwner()
										.getUniqueId(),
								BukkitProtect.Plugin.UTEntities)) {
					return;
				} else {
					Event.setCancelled(true);
				}
			}
		if (Event.getBlock().getType() == Material.SOIL) {
			if (!(Event.getEntity() instanceof Player)) {
				if (!BukkitProtect.Plugin.getConfig().getBoolean(
						"EntitiesTrampleCrops"))
					Event.setCancelled(true);
			}
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void EntityChangeBlock(EntityChangeBlockEvent Event) {
		if (Event.getEntity() == null)
			return;
		if (!BukkitProtect.Plugin.getConfig().getBoolean("EndermenBlockChange")
				&& Event.getEntityType() == EntityType.ENDERMAN)
			Event.setCancelled(true);
		if (!BukkitProtect.Plugin.getConfig().getBoolean("ZombiesBreakDoors")
				&& Event.getEntityType() == EntityType.ZOMBIE)
			Event.setCancelled(true);
		if (!BukkitProtect.Plugin.getConfig().getBoolean("WitherBlockDamage")
				&& Event.getEntityType() == EntityType.WITHER)
			Event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void EntityExplode(EntityExplodeEvent Event) {
		if (Event.getEntity() == null)
			return;
		if (!BukkitProtect.Plugin.getConfig().getBoolean("TNTBlockDamage")
				&& Event.getEntityType() == EntityType.PRIMED_TNT)
			Event.setCancelled(true);
		if (!BukkitProtect.Plugin.getConfig().getBoolean("TNTCartBlockDamage")
				&& Event.getEntityType() == EntityType.MINECART_TNT)
			Event.setCancelled(true);
		if (!BukkitProtect.Plugin.getConfig().getBoolean("CreeperBlockDamage")
				&& Event.getEntityType() == EntityType.CREEPER)
			Event.setCancelled(true);
		if (!BukkitProtect.Plugin.getConfig().getBoolean("GhastBlockDamage")
				&& Event.getEntityType() == EntityType.FIREBALL)
			Event.setCancelled(true);
		if (!BukkitProtect.Plugin.getConfig().getBoolean("WitherBlockDamage")
				&& Event.getEntityType() == EntityType.WITHER)
			Event.setCancelled(true);
		if (!BukkitProtect.Plugin.getConfig().getBoolean(
				"WitherHeadBlockDamage")
				&& Event.getEntityType() == EntityType.WITHER_SKULL)
			Event.setCancelled(true);
		if (!BukkitProtect.Plugin.getConfig().getBoolean(
				"EnderCrystalBlockDamage")
				&& Event.getEntityType() == EntityType.ENDER_CRYSTAL)
			Event.setCancelled(true);
		if (!BukkitProtect.Plugin.getConfig().getBoolean(
				"EnderDragonBlockDamage")
				&& Event.getEntityType() == EntityType.COMPLEX_PART)
			Event.setCancelled(true);
		if (!BukkitProtect.Plugin.getConfig().getBoolean(
				"EnderDragonBlockDamage")
				&& Event.getEntityType() == EntityType.ENDER_DRAGON)
			Event.setCancelled(true);
	}

}
