package io.github.elitejynx.BukkitProtect.Util;

import io.github.elitejynx.BukkitProtect.BukkitProtect;
import io.github.elitejynx.BukkitProtect.Protections.Region;
import io.github.elitejynx.BukkitProtect.Protections.Tag;
import io.github.elitejynx.BukkitProtect.Protections.UserType;

import java.lang.reflect.Field;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Openable;

public class Util {

	public static void sendMessage(Player Plr, String Path, Player Target,
			ItemStack Item) {
		String Message = BukkitProtect.Plugin.getMessages().getString(Path);
		BukkitProtect.Plugin.getLogger().info(Message);
		if (Message != null && Message != "") {
			String[] ChatColors = Message.split("\\(");
			for (String color : ChatColors) {
				try {
					color = color.split("\\)")[0];
					Field Color = ChatColor.class.getField(color.toUpperCase());
					if (Color != null)
						Message = Message.replace("(" + color + ")",
								Color.get(ChatColor.RESET).toString());
				} catch (Exception e) {
				}
			}
			Message = Message.replace("@p", Plr.getName());
			if (Target != null)
				Message = Message.replace("@t", Target.getName());
			if (Item != null)
				Message = Message.replace("@i", Item.getItemMeta()
						.getDisplayName());
			Plr.sendMessage(Message);
		}
	}

	public static boolean isItemRod(ItemStack Item) {
		if (!BukkitProtect.Plugin.RodTypes.contains(Item.getType()))
			return false;
		if (Item.getItemMeta().getLore() == null)
			return false;
		if (!Item.getItemMeta().getLore().get(0).equals("Protect your land"))
			return false;
		return true;
	}

	public static String replaceLast(String text, String regex,
			String replacement) {
		return text.replaceFirst("(?s)(.*)" + regex, "$1" + replacement);
	}

	public static boolean isTagAndValue(String Name, String Value) {
		for (Tag tag : BukkitProtect.Plugin.Tags) {
			if (tag.getName().equalsIgnoreCase(Name)) {
				boolean isValue = false;
				for (String type : tag.getValues()) {
					if (type.equalsIgnoreCase("String")) {
						isValue = true;
					} else if (type.equalsIgnoreCase("Number")) {
						try {
							Float.parseFloat(Value);
							isValue = true;
						} catch (Exception e) {

						}
					} else if (type.equalsIgnoreCase(Value)) {
						isValue = true;
					}
				}
				return isValue;
			}
		}
		return false;
	}

	public static boolean isTag(String Name) {
		for (Tag tag : BukkitProtect.Plugin.Tags) {
			if (tag.getName().equalsIgnoreCase(Name)) {
				return true;
			}
		}
		return false;
	}

	public static Tag parseTag(String Name) {
		for (Tag tag : BukkitProtect.Plugin.Tags) {
			if (tag.getName().equalsIgnoreCase(Name)) {
				return tag;
			}
		}
		return null;
	}

	public static boolean poweringDoor(Block block) {
		return block.getRelative(BlockFace.NORTH).getState().getData() instanceof Openable
				|| block.getRelative(BlockFace.SOUTH).getState().getData() instanceof Openable
				|| block.getRelative(BlockFace.EAST).getState().getData() instanceof Openable
				|| block.getRelative(BlockFace.WEST).getState().getData() instanceof Openable
				|| block.getRelative(BlockFace.DOWN).getState().getData() instanceof Openable
				|| block.getRelative(BlockFace.UP).getState().getData() instanceof Openable;
	}

	public static UserType parseUserType(String str) {
		for (UserType Type : BukkitProtect.Plugin.Types) {
			if (Type.getName().equalsIgnoreCase(str)) {
				return Type;
			}
		}
		return null;
	}

	public static Region regionfromString(String str, String str2) {
		String[] Splits = str.split("\\:");
		Location loc = new Location(BukkitProtect.Plugin.getServer().getWorld(
				Splits[0]), 0, 0, 0);
		loc.setX(Double.parseDouble(Splits[1]));
		loc.setY(Double.parseDouble(Splits[2]));
		loc.setZ(Double.parseDouble(Splits[3]));
		Splits = str2.split("\\:");
		Location loc2 = new Location(BukkitProtect.Plugin.getServer().getWorld(
				Splits[0]), 0, 0, 0);
		loc2.setX(Double.parseDouble(Splits[1]));
		loc2.setY(Double.parseDouble(Splits[2]));
		loc2.setZ(Double.parseDouble(Splits[3]));
		Region cube = new Region(loc, loc2);
		return cube;
	}

	public static String loc2str(Location loc) {
		return loc.getWorld().getName() + ":" + loc.getBlockX() + ":"
				+ loc.getBlockY() + ":" + loc.getBlockZ();
	}

	public static boolean isBlockSolid(Block block) {
		return block.getType().isOccluding()
				|| block.getType() == Material.SOIL;
	}

	public static Block GetLowestBlockRelative(Location Loc, Location rLoc) {
		Location loc = Loc.clone();
		Location rloc = rLoc.clone();
		loc.setY(rloc.getY());
		Block Use = loc.getWorld().getBlockAt(loc);
		if (isBlockSolid(Use) || Use.isLiquid()) {
			return Use;
		} else {
			while (!isBlockSolid(Use) && !Use.isLiquid() && loc.getBlockY() > 0) {
				Use = loc.getWorld().getBlockAt(loc.add(0, -1, 0));
			}
			return loc.getWorld().getBlockAt(loc);
		}
	}

	public static Block GetLowestBlock(Location Loc) {
		Location loc = Loc.clone();
		Block Use = loc.getWorld().getBlockAt(loc);
		if (isBlockSolid(Use) || Use.isLiquid()) {
			return Use;
		} else {
			while (!isBlockSolid(Use) && !Use.isLiquid() && loc.getBlockY() > 0) {
				Use = loc.getWorld().getBlockAt(loc.add(0, -1, 0));
			}
			return loc.getWorld().getBlockAt(loc);
		}
	}
}
