package io.github.elitejynx.BukkitProtect.Commands;

import io.github.elitejynx.BukkitProtect.BukkitProtect;
import io.github.elitejynx.BukkitProtect.Protections.ProtectionZone;
import io.github.elitejynx.BukkitProtect.Protections.Tag;
import io.github.elitejynx.BukkitProtect.Protections.UserType;
import io.github.elitejynx.BukkitProtect.Util.Util;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

public class TabHandler implements TabCompleter {

	protected BukkitProtect Plugin;

	public TabHandler(BukkitProtect plugin) {
		Plugin = plugin;
	}

	@Override
	public ArrayList<String> onTabComplete(CommandSender Sender, Command Cmd,
			String Label, String[] Args) {
		if (Cmd.getName().equalsIgnoreCase("giverod")
				|| Cmd.getName().equalsIgnoreCase("setowner")
				|| Cmd.getName().equalsIgnoreCase("transfer")
				|| Cmd.getName().equalsIgnoreCase("removeallprotections")
				|| Cmd.getName().equalsIgnoreCase("getland")
				|| Cmd.getName().equalsIgnoreCase("setland")
				|| Cmd.getName().equalsIgnoreCase("giveland")) {
			if (Args.length == 1) {
				return null;
			}
		} else if (Cmd.getName().equalsIgnoreCase("addUsers")) {
			if (Args.length == 1) {
				return null;
			} else if (Args.length == 2) {
				ArrayList<String> ArrayList = new ArrayList<String>();
				for (UserType UT : Plugin.Types) {
					if (UT.getName().toLowerCase()
							.startsWith(Args[Args.length - 1].toLowerCase()))
						ArrayList.add(UT.getName());
				}
				return ArrayList;
			}
		} else if (Cmd.getName().equalsIgnoreCase("addtag")) {
			if (Args.length == 1) {
				ArrayList<String> ArrayList = new ArrayList<String>();
				for (Tag tag : Plugin.Tags) {
					if (tag.getName().toLowerCase()
							.startsWith(Args[Args.length - 1].toLowerCase()))
						ArrayList.add(tag.getName());
				}
				return ArrayList;
			} else if (Args.length == 2) {
				ArrayList<String> ArrayList = new ArrayList<String>();
				if (Util.parseTag(Args[0]) != null) {
					ArrayList = Util.parseTag(Args[0]).getValues();
				}
				return ArrayList;
			}
		} else if (Cmd.getName().equalsIgnoreCase("removeUsers")) {
			if (BukkitProtect.Timers.PlayerSelectedZone.containsKey(Sender)) {
				if (Args.length == 1) {
					ArrayList<String> ArrayList = new ArrayList<String>();
					for (UUID plrid : ((ProtectionZone) BukkitProtect.Timers.PlayerSelectedZone
							.get(Sender).keySet().toArray()[0]).getUsers()
							.keySet()) {
						String plr = Plugin.getServer().getPlayer(plrid)
								.getName();
						if (plr.toLowerCase().startsWith(Args[0].toLowerCase()))
							ArrayList.add(plr);
					}
					return ArrayList;
				} else if (Args.length == 2) {
					if (((ProtectionZone) BukkitProtect.Timers.PlayerSelectedZone
							.get(Sender).keySet().toArray()[0]).getUsers()
							.containsKey(Args[0].toLowerCase())) {
						ArrayList<String> ArrayList = new ArrayList<String>();
						for (String UT : ((ProtectionZone) BukkitProtect.Timers.PlayerSelectedZone
								.get(Sender).keySet().toArray()[0]).getUsers()
								.get(Args[0].toLowerCase())) {
							if (UT.toLowerCase().startsWith(
									Args[1].toLowerCase()))
								ArrayList.add(UT);
						}
						return ArrayList;
					}
				}
			}
		} else if (Cmd.getName().equalsIgnoreCase("removetag")) {
			if (BukkitProtect.Timers.PlayerSelectedZone.containsKey(Sender)) {
				if (Args.length == 1) {
					ArrayList<String> ArrayList = new ArrayList<String>();
					for (String tag : ((ProtectionZone) BukkitProtect.Timers.PlayerSelectedZone
							.get(Sender).keySet().toArray()[0]).getTags()
							.keySet()) {
						if (tag.toLowerCase().startsWith(Args[0].toLowerCase()))
							ArrayList.add(tag);
					}
					return ArrayList;
				}
			}
		}
		return new ArrayList<String>();
	}
}
