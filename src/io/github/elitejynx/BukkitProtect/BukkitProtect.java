package io.github.elitejynx.BukkitProtect;

import io.github.elitejynx.BukkitProtect.Commands.CommandHandler;
import io.github.elitejynx.BukkitProtect.Commands.TabHandler;
import io.github.elitejynx.BukkitProtect.Events.BlockEventHandler;
import io.github.elitejynx.BukkitProtect.Events.EntityEventHandler;
import io.github.elitejynx.BukkitProtect.Events.PlayerEventHandler;
import io.github.elitejynx.BukkitProtect.Events.SpamHandler;
import io.github.elitejynx.BukkitProtect.Events.WorldEventHandler;
import io.github.elitejynx.BukkitProtect.Protections.Protection;
import io.github.elitejynx.BukkitProtect.Protections.ProtectionWorld;
import io.github.elitejynx.BukkitProtect.Protections.ProtectionZone;
import io.github.elitejynx.BukkitProtect.Protections.Region;
import io.github.elitejynx.BukkitProtect.Protections.Tag;
import io.github.elitejynx.BukkitProtect.Protections.UserType;
import io.github.elitejynx.BukkitProtect.Util.Util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author EliteJynx
 */
@SuppressWarnings("deprecation")
public class BukkitProtect extends JavaPlugin {
	// The class handling PVP
	public static TimerHandlers Timers;
	public static BukkitProtect Plugin;
	public static CommandHandler Cmds;
	public static TabHandler Tabs;

	// Files
	public File UsersFolder;
	public FileConfiguration Config;
	public FileConfiguration Messages;

	// User storage
	public HashMap<World, ArrayList<ProtectionZone>> Protections = new HashMap<World, ArrayList<ProtectionZone>>();
	/**
	 * TODO Finish this
	 */
	public ArrayList<ProtectionWorld> WorldSettings = new ArrayList<ProtectionWorld>();
	public HashMap<String, Integer> LandOwned = new HashMap<String, Integer>();

	// Values
	public ArrayList<Tag> Tags = new ArrayList<Tag>();
	public ArrayList<Tag> WorldTags = new ArrayList<Tag>();
	public ArrayList<UserType> Types = new ArrayList<UserType>();

	public UserType UTAccess = new UserType("Access",
			"Allows the use of blocks to access the area",
			Material.STAINED_CLAY, 8, Material.STAINED_CLAY, 0, 1, false);
	public UserType UTEntities = new UserType("Entities",
			"Allows the use of entities", Material.STAINED_CLAY, 7,
			Material.STAINED_CLAY, 12, 2, false);
	public UserType UTBuildBlocks = new UserType("BuildBlocks",
			"Allows the building of blocks", Material.STAINED_CLAY, 13,
			Material.STAINED_CLAY, 5, 3, false);
	public UserType UTUseBlocks = new UserType("UseBlocks",
			"Allows the use of blocks", Material.STAINED_CLAY, 11,
			Material.STAINED_CLAY, 3, 3, false);
	public UserType UTModerator = new UserType("Moderator",
			"Allows the use of commands", Material.STAINED_CLAY, 1,
			Material.STAINED_CLAY, 4, 5, true);

	public ArrayList<Material> RodTypes = new ArrayList<Material>();

	public ItemStack RodA;

	public void RemoveAllProtections(UUID User) {
		for (World world : Protections.keySet()) {
			for (ProtectionZone Zone : Protections.get(world)) {
				if (Zone.getOwner().equals(User)) {
					RemoveProtection(Zone);
				}
			}
		}
	}

	public void AddProtection(World world, ProtectionZone Zone) {
		ArrayList<ProtectionZone> Zones = Protections.get(world);
		if (Zones == null)
			Zones = new ArrayList<ProtectionZone>();
		Zones.add(Zone);
		Protections.put(world, Zones);
	}

	public void RemoveProtection(ProtectionZone Zone) {
		File ProtFile = new File(Zone.getID());
		ProtFile.delete();
		Protections.get(Zone.getCube().getCorner1().getWorld()).remove(Zone);
	}

	public void RemoveUser(UUID User) {
		RemoveAllProtections(User);
		File UserFile = new File(UsersFolder, User.toString());
		for (File file : UserFile.listFiles()) {
			file.delete();
		}
		UserFile.delete();
		LandOwned.remove(User);
	}

	public ItemStack addRod(ItemStack baseRod, String Name, int MaxUses,
			int Level, Material baseMaterial) {
		ItemStack Rod = baseRod.clone();
		ItemMeta RodMeta = Rod.getItemMeta();
		RodMeta.setDisplayName(Name);
		ArrayList<String> Lore = new ArrayList<String>();
		Lore.add("Protect your land");
		Lore.add(MaxUses + " / " + MaxUses);
		RodMeta.setLore(Lore);
		Rod.setItemMeta(RodMeta);
		Rod.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, Level);
		if (baseMaterial != null) {
			ShapedRecipe RodRecipe = new ShapedRecipe(Rod);
			RodRecipe.shape(" GG", " SG", "C  ");
			RodRecipe.setIngredient('G', Material.GLASS);
			RodRecipe.setIngredient('S', baseRod.getType());
			RodRecipe.setIngredient('C', baseMaterial);
			getServer().addRecipe(RodRecipe);
		}
		if (!RodTypes.contains(baseRod.getType()))
			RodTypes.add(baseRod.getType());
		return Rod;
	}

	public void setupRods() {
		ItemStack baseStick = new ItemStack(Material.STICK);
		ItemStack baseRod = new ItemStack(Material.BLAZE_ROD);
		addRod(baseStick, "Rod of Stone", 4, 1, Material.COBBLESTONE);
		addRod(baseStick, "Rod of Iron", 10, 2, Material.IRON_INGOT);
		addRod(baseStick, "Rod of Gold", 25, 3, Material.GOLD_INGOT);
		addRod(baseStick, "Rod of Diamond", 40, 4, Material.DIAMOND);
		addRod(baseRod, "Rod of Obsidian", 50, 5, Material.OBSIDIAN);
		RodA = addRod(baseRod, "Rod of the Admin", -1, 6, null);
	}

	public void addTag(String name, String desc, String... Values) {
		Tag tag = new Tag(name, desc);
		for (String value : Values) {
			tag.addValues(value.toLowerCase());
		}
		Tags.add(tag);
	}

	public void addWorldTag(String name, String desc, String... Values) {
		Tag tag = new Tag(name, desc);
		for (String value : Values) {
			tag.addValues(value.toLowerCase());
		}
		WorldTags.add(tag);
	}

	public void addUserType(UserType Type) {
		Types.add(Type);
	}

	public void addCommand(String Label, String Permission) {
		PluginCommand cmd = getCommand(Label);
		cmd.setExecutor(Cmds);
		cmd.setPermission(Permission);
		cmd.setPermissionMessage(ChatColor.DARK_RED + "You don't have "
				+ cmd.getPermission());
		cmd.setTabCompleter(Tabs);
	}

	public void setupTags() {
		addTag("PVP", "Prevent PVP", "true", "false");
		addTag("Fire", "Prevent fire spread", "true", "false");
		addTag("Ice", "Prevent ice melting or forming", "true", "false");
		addTag("Snow", "Prevent snow melting or forming", "true", "false");
		addTag("EntitySpawn", "Prevent entities spawning", "true", "false");
		addTag("ServerOwned",
				"Prevents this protection from being counted in the owners land blocks",
				"true");

		addWorldTag("FireBurn", "Prevents fires burning blocks in the world",
				"true", "false");
	}

	public void setupUserTypes() {
		addUserType(UTAccess);
		addUserType(UTEntities);
		addUserType(UTBuildBlocks);
		addUserType(UTUseBlocks);
		addUserType(UTModerator);
	}

	public void setupHandlers() {
		getServer().getPluginManager().registerEvents(new PlayerEventHandler(),
				this);
		getServer().getPluginManager().registerEvents(new EntityEventHandler(),
				this);
		getServer().getPluginManager().registerEvents(new BlockEventHandler(),
				this);
		getServer().getPluginManager().registerEvents(new WorldEventHandler(),
				this);
		getServer().getPluginManager().registerEvents(new SpamHandler(), this);
		Timers = new TimerHandlers(this);
		getServer().getPluginManager().registerEvents(Timers, this);
		new UpdateHandler(this, 68440, getFile(),
				UpdateHandler.UpdateType.DEFAULT, true);
		Cmds = new CommandHandler(this);
		Tabs = new TabHandler(this);
	}

	public void setupCommands() {
		addCommand("GiveRod", "BukkitProtect.Commands.GiveRod");
		addCommand("ReloadConfig", "BukkitProtect.Commands.ReloadConfig");
		addCommand("AddUsers", "BukkitProtect.Commands.Users");
		addCommand("RemoveUsers", "BukkitProtect.Commands.Users");
		addCommand("GetUsers", "BukkitProtect.Commands.Users");
		addCommand("SetOwner", "BukkitProtect.Protections.EditOthers");
		addCommand("Transfer", "BukkitProtect.Commands.Transfer");
		addCommand("Accept", "BukkitProtect.Commands.Accept");
		addCommand("RemoveProtection",
				"BukkitProtect.Commands.RemoveProtections");
		addCommand("RemoveAllProtections",
				"BukkitProtect.Commands.RemoveProtections");
		addCommand("AddTag", "BukkitProtect.Commands.Tag");
		addCommand("RemoveTag", "BukkitProtect.Commands.Tag");
		addCommand("GetTags", "BukkitProtect.Commands.Tag");
		addCommand("GiveLand", "BukkitProtect.Commands.AdminLand");
		addCommand("SetLand", "BukkitProtect.Commands.AdminLand");
		addCommand("GetLand", "BukkitProtect.Commands.GetLand");
		addCommand("Stuck", "BukkitProtect.Commands.Stuck");
	}

	public void loadUserFromFile(String Plr) {
		File UserFile = new File(UsersFolder, Plr + ".yml");
		if (UserFile.exists()) {
			if (!LandOwned.containsKey(Plr)) {
				try {
					BufferedReader ois = new BufferedReader(new FileReader(
							UserFile));

					int c;
					StringBuilder Builder = new StringBuilder();

					while ((c = ois.read()) != -1) {
						Builder.append((char) c);
					}
					String Land = Builder.toString();
					ois.close();
					LandOwned.put(Plr, Integer.parseInt(Land));
				} catch (Exception e) {
				}
			}
		}
	}

	public void loadProtectionsFromFile(File WorldFolder, World world) {
		if (WorldFolder.exists()) {
			if (!Protections.containsKey(world)) {
				ArrayList<ProtectionZone> WorldProts = new ArrayList<ProtectionZone>();
				for (File Prots : WorldFolder.listFiles()) {
					if (!Prots.getName().contains("error")
							&& !Prots.getName().equalsIgnoreCase(
									"WorldSettings.yml")) {
						try {
							BufferedReader ois = new BufferedReader(
									new FileReader(Prots));

							int c;
							StringBuilder Builder = new StringBuilder();

							while ((c = ois.read()) != -1) {
								Builder.append((char) c);
							}
							String Protection = Builder.toString();
							ois.close();
							ProtectionZone Zone = new ProtectionZone(null, null);
							try {
								Zone = Zone.fromString(Protection);
								Zone.setID(Prots.getPath());
							} catch (Exception e) {
							}
							if (Zone.getCube() != null) {
								WorldProts.add(Zone);
							} else {
								getLogger().warning(
										Prots.getPath() + " is corrupt!");
								File CorruptFile = new File(Prots.getPath()
										+ ".error");
								Prots.renameTo(CorruptFile);
							}
						} catch (Exception e) {
						}
					}
				}
				Protections.put(world, WorldProts);
			}
		}
	}

	public void loadDB() {
		for (World world : getServer().getWorlds()) {
			File ProtFolder = new File(world.getWorldFolder(), "Protections");
			ProtFolder.mkdir();
			File ProtWorldFile = new File(ProtFolder, "WorldSettings.yml");
			ProtectionWorld ProtWorld = new ProtectionWorld(world);
			loadProtectionsFromFile(ProtFolder, world);
			if (ProtWorldFile.exists()) {
				try {
					BufferedReader ois = new BufferedReader(new FileReader(
							ProtWorldFile));
					int c;
					StringBuilder Builder = new StringBuilder();

					while ((c = ois.read()) != -1) {
						Builder.append((char) c);
					}
					String Total = Builder.toString();
					ois.close();
					ProtWorld.fromString(Total, world);
				} catch (Exception e) {
				}
			} else {
				ProtWorld.toDefaults();
			}
			WorldSettings.add(ProtWorld);
		}

		if (UsersFolder.exists()) {
			for (Player Plr : getServer().getOnlinePlayers()) {
				loadUserFromFile(Plr.getName());
			}
		} else {
			getLogger()
					.severe("Could not create the protections folder! Disabling BukkitProtect");
			setEnabled(false);
		}
	}

	public void saveDB() {
		for (World world : Protections.keySet()) {
			File ProtFolder = new File(world.getWorldFolder(), "Protections");
			ProtFolder.mkdir();
			int UseNum = 0;
			for (int i = 0; i < Protections.get(world).size(); i++) {
				try {
					File Corrupt = new File(ProtFolder, UseNum + ".yml.error");
					while (Corrupt.exists()) {
						UseNum++;
						Corrupt = new File(ProtFolder, UseNum + ".yml.error");
					}
					BufferedWriter oos = new BufferedWriter(new FileWriter(
							new File(ProtFolder, UseNum + ".yml")));
					oos.write(Protections.get(world).get(i).toString());
					oos.close();
					UseNum++;
				} catch (Exception e) {
				}

			}
		}
		for (String Player : LandOwned.keySet()) {
			File UserFile = new File(UsersFolder.getPath(), Player + ".yml");
			try {
				BufferedWriter oos = new BufferedWriter(
						new FileWriter(UserFile));
				oos.write(LandOwned.get(Player).toString());
				oos.close();
			} catch (Exception e) {
			}
		}
		for (World world : getServer().getWorlds()) {
			ProtectionWorld ProtWorld = null;
			for (ProtectionWorld Prot : WorldSettings) {
				if (Prot.getProtWorld() == world)
					ProtWorld = Prot;
			}
			if (ProtWorld != null) {
				File ProtFolder = new File(world.getWorldFolder(),
						"Protections");
				ProtFolder.mkdir();
				File ProtWorldFile = new File(ProtFolder, "WorldSettings.yml");
				try {
					BufferedWriter oos = new BufferedWriter(new FileWriter(
							ProtWorldFile));
					oos.write(ProtWorld.toString());
					oos.close();
				} catch (Exception e) {
				}
			}
		}
	}

	@Override
	public void reloadConfig() {
		File ConfigFile = new File(getDataFolder(), "config.yml");
		Config = YamlConfiguration.loadConfiguration(ConfigFile);
		if (!ConfigFile.exists())
			saveResource("config.yml", false);

		InputStream defConfigStream = getResource("config.yml");
		if (defConfigStream != null) {
			YamlConfiguration defConfig = YamlConfiguration
					.loadConfiguration(defConfigStream);

			Config.setDefaults(defConfig);
		}
	}

	public void reloadMessages() {
		File MessagesFile = new File(getDataFolder(), "messages.yml");
		Messages = YamlConfiguration.loadConfiguration(MessagesFile);
		if (!MessagesFile.exists())
			saveResource("messages.yml", false);

		InputStream defMessagesStream = getResource("messages.yml");
		if (defMessagesStream != null) {
			YamlConfiguration defMessages = YamlConfiguration
					.loadConfiguration(defMessagesStream);

			Messages.setDefaults(defMessages);
		}
	}

	@Override
	public FileConfiguration getConfig() {
		if (Config == null) {
			reloadConfig();
		}
		return Config;
	}

	public FileConfiguration getMessages() {
		if (Messages == null) {
			reloadMessages();
		}
		return Messages;
	}

	public void setupConfig() {
		if (!new File(getDataFolder(), "config.yml").exists())
			saveResource("config.yml", false);
		if (!new File(getDataFolder(), "messages.yml").exists())
			saveResource("messages.yml", false);
	}

	@Override
	public void onEnable() {
		Plugin = this;

		UsersFolder = new File(getDataFolder(), "Users");
		UsersFolder.mkdir();

		setupTags();

		setupUserTypes();

		loadDB();

		if (isEnabled()) {
			setupConfig();

			setupRods();

			setupHandlers();

			setupCommands();

			for (Player Plr : getServer().getOnlinePlayers()) {
				BukkitProtect.Plugin.loadUserFromFile(Plr.getName());
				if (BukkitProtect.Plugin.getConfig().getBoolean("BuyableLand")) {
					if (!BukkitProtect.Plugin.LandOwned.containsKey(Plr
							.getName())) {
						BukkitProtect.Plugin.LandOwned.put(
								Plr.getName(),
								BukkitProtect.Plugin.getConfig().getInt(
										"StartLand"));
					}
				}
			}
		}
	}

	@Override
	public void onDisable() {
		for (Player Plr : getServer().getOnlinePlayers()) {
			if (Timers.UpdateBlock.containsKey(Plr)) {
				HashMap<Block, Integer> Blocks = Timers.UpdateBlock.get(Plr);
				for (Block block : Blocks.keySet()) {
					block.getState().update();
				}
			}
		}
		saveDB();
	}

	public int getTotalLandUsed(Player Plr) {
		if (Protections.containsKey(Plr.getName())) {
			ArrayList<ProtectionZone> Zones = Protections.get(Plr.getName());
			int Total = 0;
			for (ProtectionZone Zone : Zones) {
				if (!Zone.getTag("ServerOwned").equalsIgnoreCase("true")) {
					Total = Total + Zone.getCube().getSize();
				}
			}
			return Total;
		} else {
			return 0;
		}
	}

	public ProtectionZone isInsideBiggestProtection(Location loc) {
		if (Protections.isEmpty())
			return null;
		Location Loc = loc.clone();
		ProtectionZone Zone = null;
		for (ArrayList<ProtectionZone> Zones : Protections.values()) {
			for (ProtectionZone ProtZone : Zones) {
				if (Zone == null) {
					if (ProtZone.getCube().isInside(Loc, false))
						Zone = ProtZone;
				} else {
					if (Zone.getCube().getSize() < ProtZone.getCube().getSize()) {
						if (ProtZone.getCube().isInside(Loc, false))
							Zone = ProtZone;
					}
				}
			}
		}
		return Zone;
	}

	public ProtectionZone isInsideProtection(Location loc) {
		if (Protections.isEmpty())
			return null;
		Location Loc = loc.clone();
		ProtectionZone Zone = null;
		for (ArrayList<ProtectionZone> Zones : Protections.values()) {
			for (ProtectionZone ProtZone : Zones) {
				if (Zone == null) {
					if (ProtZone.getCube().isInside(Loc, false))
						Zone = ProtZone;
				} else {
					if (Zone.getCube().getSize() > ProtZone.getCube().getSize()) {
						if (ProtZone.getCube().isInside(Loc, true))
							Zone = ProtZone;
					} else {
						if (ProtZone.getCube().isInside(Loc, false)
								&& !Zone.getCube().isInside(Loc, true)) {
							Zone = ProtZone;
						}
					}
				}
			}
		}
		return Zone;
	}

	public void CornerRod(Player Plr, Location blockLoc, ItemStack Rod) {
		Block Left = Util.GetLowestBlock(blockLoc.clone().add(1, 0, 0));
		Block Right = Util.GetLowestBlock(blockLoc.clone().add(-1, 0, 0));
		Block Center = Util.GetLowestBlock(blockLoc.clone());
		Block Forward = Util.GetLowestBlock(blockLoc.clone().add(0, 0, 1));
		Block Backward = Util.GetLowestBlock(blockLoc.clone().add(0, 0, -1));
		Plr.sendBlockChange(Left.getLocation(), Material.STAINED_CLAY, (byte) 5);
		Plr.sendBlockChange(Right.getLocation(), Material.STAINED_CLAY,
				(byte) 5);
		Plr.sendBlockChange(Center.getLocation(), Material.STAINED_CLAY,
				(byte) 13);
		Plr.sendBlockChange(Forward.getLocation(), Material.STAINED_CLAY,
				(byte) 5);
		Plr.sendBlockChange(Backward.getLocation(), Material.STAINED_CLAY,
				(byte) 5);
		Location EffectsLoc = Center.getLocation().add(0, 1, 0);
		ItemMeta RodMeta = Rod.getItemMeta();
		boolean infinite = false;
		if (RodMeta.getLore().get(1).split("/").length == 2) {
			String[] Nums = RodMeta.getLore().get(1).replace(" ", "")
					.split("/");
			int Num1 = Integer.parseInt(Nums[0]);
			if (Num1 > 1) {
				Num1 = Num1 - 1;
				ArrayList<String> Lore = new ArrayList<String>();
				Lore.add(RodMeta.getLore().get(0));
				Lore.add(Num1 + " / " + Nums[1]);
				RodMeta.setLore(Lore);
				Rod.setItemMeta(RodMeta);
				Plr.playEffect(EffectsLoc, Effect.ENDER_SIGNAL, 1);
				Plr.playSound(EffectsLoc, Sound.ENDERMAN_TELEPORT, 1, 1);
			} else if (Num1 == -1) {
				Plr.playEffect(EffectsLoc, Effect.ENDER_SIGNAL, 1);
				Plr.playSound(EffectsLoc, Sound.ENDERMAN_TELEPORT, 1, 1);
				infinite = true;
			} else {
				Util.sendMessage(Plr, "RodBreak", null, Rod);
				Plr.getInventory().setItemInHand(new ItemStack(Material.STICK));
				Plr.playEffect(EffectsLoc, Effect.ENDER_SIGNAL, 1);
				Plr.getWorld().playSound(Plr.getLocation(), Sound.ITEM_BREAK,
						1, 1);
			}
		}
		HashMap<Block, Integer> Blocks = Timers.UpdateBlock.get(Plr);
		if (Blocks == null)
			Blocks = new HashMap<Block, Integer>();
		Blocks.put(Left, 60);
		Blocks.put(Right, 60);
		Blocks.put(Center, 60);
		Blocks.put(Forward, 60);
		Blocks.put(Backward, 60);
		Timers.UpdateBlock.put(Plr, Blocks);
		HashMap<Location, Integer> Selections = Timers.PlayerSelection.get(Plr);
		if (Selections == null)
			Selections = new HashMap<Location, Integer>();
		Selections.put(Center.getLocation(), 60);
		if (Selections.size() >= 3) {
			Selections.remove(Selections.keySet().toArray()[0]);
		} else if (Selections.size() == 2) {
			Location Sel1 = null;
			Location Sel2 = null;
			if (((Location) Selections.keySet().toArray()[0]).equals(Center
					.getLocation())) {
				Sel1 = (Location) Selections.keySet().toArray()[1];
				Sel2 = (Location) Selections.keySet().toArray()[0];
			} else {
				Sel1 = (Location) Selections.keySet().toArray()[0];
				Sel2 = (Location) Selections.keySet().toArray()[1];
			}
			ProtectionZone newProt = new ProtectionZone(new Region(Sel1, Sel2),
					Plr.getUniqueId());
			boolean resizeZone = false;
			if (Protections.containsKey(Plr.getName())) {
				for (ProtectionZone Zone : Protections.get(Plr.getName())) {
					for (Location loc : GetCorners(Zone)) {
						boolean insideZone = false;
						if (isInsideBiggestProtection(loc) == Zone) {
							if (loc.getBlockX() == Sel1.getBlockX()
									&& loc.getBlockZ() == Sel1.getBlockZ())
								insideZone = true;
						} else {
							int MinY = Math.min(Sel1.getBlockY(),
									Sel2.getBlockY());
							int MaxY = Math.max(Sel1.getBlockY(),
									Sel2.getBlockY());
							if (loc.getBlockX() == Sel1.getBlockX()
									&& loc.getBlockZ() == Sel1.getBlockZ()
									&& loc.getBlockY() >= MinY
									&& loc.getBlockY() <= MaxY)
								insideZone = true;
						}
						if (insideZone) {
							newProt = Zone.Clone();
							if (loc.getBlockX() == newProt.getCube()
									.getCorner1().getBlockX()
									&& loc.getBlockZ() == newProt.getCube()
											.getCorner1().getBlockZ()) {
								newProt.getCube().setCorner1(Sel2);
								resizeZone = true;
							} else if (loc.getBlockX() == newProt.getCube()
									.getCorner2().getBlockX()
									&& loc.getBlockZ() == newProt.getCube()
											.getCorner2().getBlockZ()) {
								newProt.getCube().setCorner2(Sel2);
								resizeZone = true;
							} else if (loc.getBlockX() == newProt.getCube()
									.getCorner3().getBlockX()
									&& loc.getBlockZ() == newProt.getCube()
											.getCorner3().getBlockZ()) {
								newProt.getCube().setCorner3(Sel2);
								resizeZone = true;
							} else if (loc.getBlockX() == newProt.getCube()
									.getCorner4().getBlockX()
									&& loc.getBlockZ() == newProt.getCube()
											.getCorner4().getBlockZ()) {
								newProt.getCube().setCorner4(Sel2);
								resizeZone = true;
							}
						}
					}
				}
			}
			int Length = newProt.getCube().getLength();
			int Width = newProt.getCube().getWidth();
			if (Length >= getConfig().getInt("MinimumZoneSize")
					&& Width >= getConfig().getInt("MinimumZoneSize")) {
				ArrayList<ProtectionZone> Intersecting = new ArrayList<ProtectionZone>();
				for (ArrayList<ProtectionZone> Zones : Protections.values()) {
					for (ProtectionZone Zone : Zones) {
						if (newProt.getCube().zonesIntersect(Zone.getCube(),
								false)) {
							if (!Zone.userHasAdminType(Plr.getUniqueId())) {
								Intersecting.add(Zone);
							} else {
								ProtectionZone InsideZone = isInsideBiggestProtection(newProt
										.getCube().getCorner1());
								if (InsideZone == Zone) {
									if (!Zone.getCube().isInside(
											newProt.getCube().getCorner1(),
											false)
											|| !Zone.getCube().isInside(
													newProt.getCube()
															.getCorner2(),
													false))
										Intersecting.add(Zone);
								} else if (newProt.getCube().zonesIntersect(
										Zone.getCube(), true)) {
									if (!Zone.getCube().isInside(
											newProt.getCube().getCorner1(),
											true)
											|| !Zone.getCube()
													.isInside(
															newProt.getCube()
																	.getCorner2(),
															true))
										Intersecting.add(Zone);
								}
							}
						}
					}
				}
				if (!Intersecting.isEmpty() && Intersecting.size() == 1
						&& resizeZone) {
					if (Intersecting.get(0) == ((ProtectionZone) Timers.PlayerSelectedZone
							.get(Plr).keySet().toArray()[0])) {
						Intersecting.remove(0);
					}
				}
				if (Intersecting.isEmpty()) {
					if (getConfig().getBoolean("BuyableLand") && !infinite) {
						if (LandOwned.containsKey(Plr.getName())) {
							int oldSize = 0;
							if (resizeZone) {
								oldSize = ((ProtectionZone) Timers.PlayerSelectedZone
										.get(Plr).keySet().toArray()[0])
										.getCube().getSize();
							}
							if ((LandOwned.get(Plr.getName())
									- getTotalLandUsed(Plr) + oldSize) < newProt
									.getCube().getSize()) {
								Plr.sendMessage("You need "
										+ (newProt.getCube().getSize() - (LandOwned
												.get(Plr.getName())
												- getTotalLandUsed(Plr) + oldSize))
										+ " more blocks of land");
								Timers.PlayerSelection.remove(Plr);
								Timers.updateFakeBlocks(Plr);
								return;
							}
						}
					}
					if (resizeZone) {
						RemoveProtection((ProtectionZone) (Timers.PlayerSelectedZone
								.get(Plr).keySet().toArray()[0]));
					}
					if (infinite)
						newProt.addTags("ServerOwned", "true");
					AddProtection(Plr.getWorld(), newProt);
					Timers.PlayerSelection.remove(Plr);
					Timers.updateFakeBlocks(Plr);
					return;
				} else {
					for (ProtectionZone Zone : Intersecting) {
						DisplayProtection(Plr, Zone.getCube().getCorner1());
					}
					Plr.sendMessage("The protection intersects another protection");
				}
			} else {
				Plr.sendMessage("The protection must be atleast "
						+ getConfig().getInt("MinimumZoneSize") + " x "
						+ getConfig().getInt("MinimumZoneSize"));
				Timers.PlayerSelection.remove(Plr);
				Timers.updateFakeBlocks(Plr);
				return;
			}
		}
		Timers.PlayerSelection.put(Plr, Selections);
	}

	public ArrayList<Location> GetCorners(ProtectionZone Zone) {
		Location Corner1 = Zone.getCube().getCorner1();
		Location Corner2 = Zone.getCube().getCorner2();
		Location Corner3 = Zone.getCube().getCorner3();
		Location Corner4 = Zone.getCube().getCorner4();

		ArrayList<Location> Corners = new ArrayList<Location>();

		if (Corner1.getBlockX() < Corner2.getBlockX()
				&& Corner1.getBlockZ() < Corner2.getBlockZ()) {
			Corners.add(Corner1);
			if (Corner3.getBlockX() < Corner4.getBlockX()) {
				Corners.add(Corner3);
				Corners.add(Corner2);
				Corners.add(Corner4);
			} else {
				Corners.add(Corner4);
				Corners.add(Corner2);
				Corners.add(Corner3);
			}
		} else if (Corner2.getBlockX() < Corner1.getBlockX()
				&& Corner2.getBlockZ() < Corner1.getBlockZ()) {
			Corners.add(Corner2);
			if (Corner3.getBlockX() < Corner4.getBlockX()) {
				Corners.add(Corner3);
				Corners.add(Corner1);
				Corners.add(Corner4);
			} else {
				Corners.add(Corner4);
				Corners.add(Corner1);
				Corners.add(Corner3);
			}
		} else if (Corner3.getBlockX() < Corner4.getBlockX()
				&& Corner3.getBlockZ() < Corner4.getBlockZ()) {
			Corners.add(Corner3);
			if (Corner1.getBlockX() < Corner2.getBlockX()) {
				Corners.add(Corner1);
				Corners.add(Corner4);
				Corners.add(Corner2);
			} else {
				Corners.add(Corner2);
				Corners.add(Corner4);
				Corners.add(Corner1);
			}
		} else {
			Corners.add(Corner4);
			if (Corner1.getBlockX() < Corner2.getBlockX()) {
				Corners.add(Corner1);
				Corners.add(Corner3);
				Corners.add(Corner2);
			} else {
				Corners.add(Corner2);
				Corners.add(Corner3);
				Corners.add(Corner1);
			}
		}

		return Corners;
	}

	public void DisplayProtection(Player Plr, Location blockLoc) {
		ProtectionZone Zone = isInsideProtection(blockLoc);
		if (Zone == null)
			return;

		Material CType = Material.STAINED_CLAY;
		int CMeta = 14;
		Material SType = Material.STAINED_CLAY;
		int SMeta = 6;
		int priority = 0;
		boolean Admin = false;

		for (UserType UType : Types) {
			if (Zone.userHasType(Plr.getUniqueId(), UType)) {
				if (UType.getPriority() > priority) {
					if (UType.getCornerDisplay() != null
							&& UType.getSideDisplay() != null) {
						CType = UType.getCornerDisplay();
						CMeta = UType.getCornerMeta();
						SType = UType.getSideDisplay();
						SMeta = UType.getSideMeta();
						priority = UType.getPriority();
					}
				}
				if (UType.isAdmin()) {
					Admin = true;
				}
			}
		}
		if (Admin) {
			HashMap<Protection, Integer> SelZone = new HashMap<Protection, Integer>();
			SelZone.put(Zone, 60);
			Timers.PlayerSelectedZone.put(Plr, SelZone);
		}
		if (Plr.hasPermission("BukkitProtect.Protection.SelectOthers")) {
			HashMap<Protection, Integer> SelZone = new HashMap<Protection, Integer>();
			SelZone.put(Zone, 60);
			Timers.PlayerSelectedZone.put(Plr, SelZone);
		}

		Location LocUseL;
		Location LocUseR;
		Location LocUseC;

		ArrayList<Location> Corners = GetCorners(Zone);

		HashMap<Block, Integer> Blocks = Timers.UpdateBlock.get(Plr);
		if (Blocks == null) {
			Blocks = new HashMap<Block, Integer>();
		}

		for (int i = 0; i < Corners.size(); i++) {

			LocUseC = Corners.get(i);
			if (i == 0) {
				LocUseR = Corners.get(i).clone().add(1, 0, 0);
				LocUseL = Corners.get(i).clone().add(0, 0, 1);
				int Length = Zone.getCube().getLength();
				int Width = Zone.getCube().getWidth();
				for (int a = 0; a < (Length - 3) / 4; a++) {
					Location LineUse = LocUseC.clone().add(3 + (2 * a), 0, 0);
					Block Line = Util.GetLowestBlockRelative(LineUse,
							Plr.getLocation());
					Plr.sendBlockChange(Line.getLocation(), SType, (byte) SMeta);
					Blocks.put(Line, 60);
				}
				for (int a = 0; a < (Width - 3) / 4; a++) {
					Location LineUse = LocUseC.clone().add(0, 0, 3 + (2 * a));
					Block Line = Util.GetLowestBlockRelative(LineUse,
							Plr.getLocation());
					Plr.sendBlockChange(Line.getLocation(), SType, (byte) SMeta);
					Blocks.put(Line, 60);
				}
			} else if (i == 1) {
				LocUseR = Corners.get(i).clone().add(1, 0, 0);
				LocUseL = Corners.get(i).clone().add(0, 0, -1);
				int Length = Zone.getCube().getLength();
				int Width = Zone.getCube().getWidth();
				for (int a = 0; a < (Length - 3) / 4; a++) {
					Location LineUse = LocUseC.clone().add(3 + (2 * a), 0, 0);
					Block Line = Util.GetLowestBlockRelative(LineUse,
							Plr.getLocation());
					Plr.sendBlockChange(Line.getLocation(), SType, (byte) SMeta);
					Blocks.put(Line, 60);
				}
				for (int a = 0; a < (Width - 3) / 4; a++) {
					Location LineUse = LocUseC.clone().subtract(0, 0,
							3 + (2 * a));
					Block Line = Util.GetLowestBlockRelative(LineUse,
							Plr.getLocation());
					Plr.sendBlockChange(Line.getLocation(), SType, (byte) SMeta);
					Blocks.put(Line, 60);
				}
			} else if (i == 2) {
				LocUseR = Corners.get(i).clone().add(-1, 0, 0);
				LocUseL = Corners.get(i).clone().add(0, 0, -1);
				int Length = Zone.getCube().getLength();
				int Width = Zone.getCube().getWidth();
				for (int a = 0; a < (Length - 3) / 4; a++) {
					Location LineUse = LocUseC.clone().subtract(3 + (2 * a), 0,
							0);
					Block Line = Util.GetLowestBlockRelative(LineUse,
							Plr.getLocation());
					Plr.sendBlockChange(Line.getLocation(), SType, (byte) SMeta);
					Blocks.put(Line, 60);
				}
				for (int a = 0; a < (Width - 3) / 4; a++) {
					Location LineUse = LocUseC.clone().subtract(0, 0,
							3 + (2 * a));
					Block Line = Util.GetLowestBlockRelative(LineUse,
							Plr.getLocation());
					Plr.sendBlockChange(Line.getLocation(), SType, (byte) SMeta);
					Blocks.put(Line, 60);
				}
			} else {
				LocUseR = Corners.get(i).clone().add(-1, 0, 0);
				LocUseL = Corners.get(i).clone().add(0, 0, 1);
				int Length = Zone.getCube().getLength();
				int Width = Zone.getCube().getWidth();
				for (int a = 0; a < (Length - 3) / 4; a++) {
					Location LineUse = LocUseC.clone().subtract(3 + (2 * a), 0,
							0);
					Block Line = Util.GetLowestBlockRelative(LineUse,
							Plr.getLocation());
					Plr.sendBlockChange(Line.getLocation(), SType, (byte) SMeta);
					Blocks.put(Line, 60);
				}
				for (int a = 0; a < (Width - 3) / 4; a++) {
					Location LineUse = LocUseC.clone().add(0, 0, 3 + (2 * a));
					Block Line = Util.GetLowestBlockRelative(LineUse,
							Plr.getLocation());
					Plr.sendBlockChange(Line.getLocation(), SType, (byte) SMeta);
					Blocks.put(Line, 60);
				}
			}

			Block Left = Util
					.GetLowestBlockRelative(LocUseL, Plr.getLocation());
			Block Right = Util.GetLowestBlockRelative(LocUseR,
					Plr.getLocation());
			Block Center = Util.GetLowestBlockRelative(LocUseC,
					Plr.getLocation());

			Plr.sendBlockChange(Left.getLocation(), SType, (byte) SMeta);
			Plr.sendBlockChange(Right.getLocation(), SType, (byte) SMeta);
			Plr.sendBlockChange(Center.getLocation(), CType, (byte) CMeta);

			Blocks.put(Left, 60);
			Blocks.put(Right, 60);
			Blocks.put(Center, 60);
		}

		Timers.UpdateBlock.put(Plr, Blocks);
	}

	public String getOverridingTag(String Tag, Location Loc) {
		ProtectionZone Zone = isInsideProtection(Loc);
		if (Zone != null) {
			ProtectionZone MainZone = isInsideBiggestProtection(Loc);
			if (Zone != MainZone) {
				if (!MainZone.getTag(Tag.toLowerCase()).equalsIgnoreCase("")) {
					return MainZone.getTag(Tag.toLowerCase());
				} else {
					return Zone.getTag(Tag.toLowerCase());
				}
			} else {
				return Zone.getTag(Tag.toLowerCase());
			}
		}
		// if (World.hasTag(Tag) != "") {
		// return World.hasTag(Tag);
		// }
		return "";
	}
}
