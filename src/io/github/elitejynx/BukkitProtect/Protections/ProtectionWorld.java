package io.github.elitejynx.BukkitProtect.Protections;

import io.github.elitejynx.BukkitProtect.Util.Util;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

import org.bukkit.World;

import com.google.common.base.Splitter;

public class ProtectionWorld extends Protection {

	protected World ProtWorld;

	public ProtectionWorld(World protworld) {
		if (protworld != null)
			setProtWorld(protworld);
	}

	@Override
	public ProtectionWorld Clone() {
		ProtectionWorld newZone = new ProtectionWorld(ProtWorld);
		newZone.setUsers(Users);
		newZone.setTags(Tags);
		return newZone;
	}

	public void toDefaults() {
		addUsers(UUID.fromString("*"), null);
	}

	@Override
	public String toString() {
		return "Users: "
				+ Util.replaceLast(Users.toString().replaceFirst("\\{", ""),
						"\\}", "")
				+ "\n"
				+ "Tags: "
				+ Util.replaceLast(Tags.toString().replaceFirst("\\{", ""),
						"\\}", "");
	}

	public ProtectionWorld fromString(String Total, World world) {
		String[] Splits = Total.split("\n");
		ProtWorld = world;
		try {
			String Split = Splits[0].replace("Users: ", "");
			Map<String, String> StringUsers = Splitter.on("], ")
					.withKeyValueSeparator("=").split(Split);
			for (String plrname : StringUsers.keySet()) {
				UUID plr = UUID.fromString(plrname);
				ArrayList<String> ListUsers = new ArrayList<String>();
				String str = StringUsers.get(plrname);
				try {
					if (str.split("\\[")[1].split("\\]").length == 1)
						for (String str2 : str.split("\\[")[1].split("\\]")[0]
								.split(", ")) {
							ListUsers.add(str2.trim());
						}
				} catch (Exception e) {
				}
				Users.put(plr, ListUsers);
			}
		} catch (Exception e) {
		}
		try {
			String Split = Splits[1].replace("Tags: ", "");
			Map<String, String> StringTags = Splitter.on(", ")
					.withKeyValueSeparator("=").split(Split);
			for (String str : StringTags.keySet()) {
				Tags.put(str, StringTags.get(str));
			}
		} catch (Exception e) {
		}
		return this;
	}

	public World getProtWorld() {
		return ProtWorld;
	}

	public void setProtWorld(World protWorld) {
		ProtWorld = protWorld;
	}
}
