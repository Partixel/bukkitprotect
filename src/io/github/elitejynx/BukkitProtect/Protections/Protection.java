package io.github.elitejynx.BukkitProtect.Protections;

import io.github.elitejynx.BukkitProtect.BukkitProtect;
import io.github.elitejynx.BukkitProtect.Util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class Protection {

	protected HashMap<UUID, ArrayList<String>> Users = new HashMap<UUID, ArrayList<String>>();
	protected HashMap<String, String> Tags = new HashMap<String, String>();

	public Protection() {
	}

	public Protection Clone() {
		Protection newZone = new Protection();
		newZone.setUsers(Users);
		newZone.setTags(Tags);
		return newZone;
	}

	public HashMap<UUID, ArrayList<String>> getUsers() {
		return Users;
	}

	public void setUsers(HashMap<UUID, ArrayList<String>> newUsers) {
		Users = newUsers;
	}

	public boolean addUsers(UUID Plr, UserType Type) {
		if (!Users.containsKey(Plr)) {
			ArrayList<String> Types = new ArrayList<String>();
			if (Type == null) {
				for (UserType UT : BukkitProtect.Plugin.Types) {
					Types.add(UT.getName().toLowerCase());
				}
			} else {
				Types.add(Type.getName().toLowerCase());
			}
			Users.put(Plr, Types);
			return true;
		} else {
			ArrayList<String> Types = Users.get(Plr);
			if (Type == null) {
				Types = new ArrayList<String>();
				for (UserType UT : BukkitProtect.Plugin.Types) {
					Types.add(UT.getName().toLowerCase());
				}
			} else {
				if (!Types.contains(Type.getName().toLowerCase())) {
					Types.add(Type.getName().toLowerCase());
				} else {
					return false;
				}
			}
			Users.put(Plr, Types);
			return true;
		}
	}

	public boolean removeUsers(UUID Plr, UserType Type) {
		if (Users.containsKey(Plr)) {
			if (Type == null) {
				Users.remove(Plr);
				return true;
			} else {
				if (Users.get(Plr).contains(Type.getName().toLowerCase())) {
					ArrayList<String> Types = Users.get(Plr);
					Types.remove(Type.getName().toLowerCase());
					if (Types.isEmpty()) {
						Users.remove(Plr);
						return true;
					} else {
						Users.put(Plr, Types);
						return true;
					}
				}
			}
		}
		return false;
	}

	public boolean addTags(String Tag, String Value) {
		if (!getTag(Tag).equalsIgnoreCase(Value)) {
			Tags.put(Tag, Value);
			return true;
		}
		return false;
	}

	public boolean removeTags(String Tag) {
		if (getTag(Tag.toLowerCase()) != "") {
			Tags.remove(Tag.toLowerCase());
			return true;
		}
		return false;
	}

	public HashMap<String, String> getTags() {
		return Tags;
	}

	public void setTags(HashMap<String, String> tags) {
		Tags = tags;
	}

	public boolean userHasAdminType(UUID Plr) {
		if (Users.containsKey(Plr)) {
			for (String str : Users.get(Plr)) {
				UserType UT = Util.parseUserType(str.toLowerCase());
				if (UT != null) {
					return UT.isAdmin();
				}
			}
		}
		return false;
	}

	public boolean userHasType(UUID Plr, UserType Type) {
		if (Users.containsKey(Plr)) {
			if (Users.get(Plr).contains(
					Type.getName().toLowerCase()))
				return true;
		} else if (Users.containsKey("*")) {
			if (Users.get("*").contains(Type.getName().toLowerCase()))
				return true;
		}
		return false;
	}

	public String getTag(String Type) {
		if (Tags.containsKey(Type)) {
			return Tags.get(Type);
		} else {
			return "";
		}
	}
}
