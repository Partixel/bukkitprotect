package io.github.elitejynx.BukkitProtect.Protections;

import io.github.elitejynx.BukkitProtect.Util.Util;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

import com.google.common.base.Splitter;

public class ProtectionZone extends Protection {

	protected UUID Owner;
	protected Region Cube;
	protected String ID = "";

	public ProtectionZone(Region cube, UUID Plr) {
		if (Plr != null)
			setOwner(Plr);
		if (cube != null)
			setCube(cube);
	}

	@Override
	public ProtectionZone Clone() {
		ProtectionZone newZone = new ProtectionZone(Cube.Clone(), Owner);
		newZone.setUsers(Users);
		newZone.setTags(Tags);
		return newZone;
	}

	@Override
	public String toString() {
		return Cube.toString()
				+ "\n"
				+ "Owner: "
				+ Owner.toString()
				+ "\n"
				+ "Users: "
				+ Util.replaceLast(Users.toString().replaceFirst("\\{", ""),
						"\\}", "")
				+ "\n"
				+ "Tags: "
				+ Util.replaceLast(Tags.toString().replaceFirst("\\{", ""),
						"\\}", "");
	}

	public ProtectionZone fromString(String Total) {
		String[] Splits = Total.split("\n");
		try {
			String Split = Splits[0].replace("Corner1: ", "");
			String Split1 = Splits[1].replace("Corner2: ", "");
			Cube = Util.regionfromString(Split.trim(), Split1.trim());
		} catch (Exception e) {
		}
		try {
			String Split = Splits[2].replace("Owner: ", "");
			Owner = UUID.fromString(Split.trim());
		} catch (Exception e) {
		}
		try {
			String Split = Splits[3].replace("Users: ", "");
			Map<String, String> StringUsers = Splitter.on("], ")
					.withKeyValueSeparator("=").split(Split);
			for (String plrname : StringUsers.keySet()) {
				UUID plr = UUID.fromString(plrname);
				ArrayList<String> ListUsers = new ArrayList<String>();
				String str = StringUsers.get(plrname);
				try {
					if (str.split("\\[")[1].split("\\]").length == 1)
						for (String str2 : str.split("\\[")[1].split("\\]")[0]
								.split(", ")) {
							ListUsers.add(str2.trim());
						}
				} catch (Exception e) {
				}
				Users.put(plr, ListUsers);
			}
		} catch (Exception e) {
		}
		try {
			String Split = Splits[4].replace("Tags: ", "");
			Map<String, String> StringTags = Splitter.on(", ")
					.withKeyValueSeparator("=").split(Split);
			for (String str : StringTags.keySet()) {
				Tags.put(str, StringTags.get(str));
			}
		} catch (Exception e) {
		}
		return this;
	}

	public Region getCube() {
		return Cube;
	}

	public void setCube(Region cube) {
		Cube = cube;
	}

	public UUID getOwner() {
		return Owner;
	}

	public void setOwner(UUID owner) {
		Owner = owner;
	}

	@Override
	public boolean addUsers(UUID Plr, UserType Type) {
		return !Owner.equals(Plr) && super.addUsers(Plr, Type);
	}

	@Override
	public boolean userHasAdminType(UUID Plr) {
		return Owner.equals(Plr) || super.userHasAdminType(Plr);
	}

	@Override
	public boolean userHasType(UUID Plr, UserType Type) {
		return Owner.equals(Plr) || super.userHasType(Plr, Type);
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}
}
